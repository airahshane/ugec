<!DOCTYPE html>
<html lang="en">
<?php include('layout/head.php') ?>

<style type="text/css">

/*PARALLAX CERTIFICATIONS*/
.parallax {
  /* The image used */
  background-image: url('image/parallax.jpg') !important;

  /* Full height */
  height: 100%;  

  /* Create the parallax scrolling effect */
  background-attachment: fixed;
  background-position: center;
  background-repeat: repeat;
  background-size: cover;
  margin-bottom: -50px;
}

.bg1 {
  background: url(image/bg1.png);
}

/*.parent {
  position: relative;
  top: 0;
  left: 0;
}*/
/*.image1 {
  position: relative;
  top: 0;
  left: 0;
  border: 1px red solid;
}*/
.image2 {
  position: absolute;
  z-index: 1;

  padding-top: 300px;
  padding-left: 360px;
/*padding: 30px;*/
}
</style>

<body class="body-color">
  <?php include('layout/navbar.php') ?>

<!--Slideshow--> 
  <div class="container-fluid p-0" data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">
    <div id="slideshow" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
      <ol class="carousel-indicators">
        <li data-target="#slideshow" data-slide-to="0" class="active"></li>
        <li data-target="#slideshow" data-slide-to="1"></li>
        <li data-target="#slideshow" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active ">
          <img class="d-block w-100 img-fluid  " src="image/slide1.jpg" alt="First slide" >
          <div class="img-fluid">
           <img class="image2" src="image/UGEC-logo-light.png"/ align="center">
             </div>

        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="image/slide2.jpg" alt="Second slide" >
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="image/slide3.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#slideshow" role="button" data-slide="prev"> 
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#slideshow" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>         
  </div>



  <!--Expertise-->
    <div class="container-fluid container-bg"><br><br><br>
        <h1 class="text-center text-gray shadow-text "  data-aos="fade-up" data-aos-once="true" data-aos-duration="1000" >EXPERTISE</h1>
        <h5 class="text-center m-5 font-weight-light text-gray" data-aos="fade-up" data-aos-once="true" data-aos-duration="2000"  style="line-height: 1.6;">UGEC is a one-stop-shop, catering to the complexity and sophistication of our customer’s needs. It encompasses the aspects of print and packaging solutions from design conceptualization to execution. With the diversity of the products and services that the company can offer we have organized ourselves into three strategic business units: Offset, Rotogravure and Print Media. In so doing, we have created focus in the formation and implementation of our strategies, promoted synergy in the workplace and streamlined supply chain activities to achieve operational excellence.</h5><br> 
    </div>

    <div class="container-fluid bg1"><br>
      <div class="card-columns bg1">
        <div class="card container-fluid bg1 " data-aos="flip-up"  data-aos-once="true" data-aos-duration="2000"  style="border: none;">
          <img class="card-img-top img-fluid shadow-img hvr-float"    src="image/2.jpg" alt="Card image" >
          <div class="card-body ">
            <p class="card-text text-justify font-weight-light text-whiteg">Our image setters are calibrated daily, using the finest equipment in the trade, and processing is exactly controlled to guarantee precise dot percentages and density</p>
            <p class="hvr-sweep-to-top btn text-whiteg" style="">PRE-PRESS OUTPUT STUDIO</p>
          </div>
        </div>
        <div class="card container-fluid bg1 " data-aos="flip-up" data-aos-once="true" data-aos-duration="2000" style="border: none;">
          <img class="card-img-top img-fluid shadow-img hvr-float" src="image/3.jpg" alt="Card image" >
          <div class="card-body">
            <p class="card-text text-justify font-weight-light text-whiteg"  >Our experienced Creative Marketing Specialists can take your ideas and comprehensives, communicate closely and assist in conceptualizing designs that will help in your product and</p>
            <p class="hvr-sweep-to-top btn text-whiteg" style="">CREATIVE MARKETING</p>
          </div>
        </div>
        <div class="card container-fluid bg1"   data-aos="flip-up"  data-aos-duration="2000"   style="border: none;">
          <img class="card-img-top img-fluid shadow-img hvr-float" src="image/1.jpg" alt="Card image" >
          <div class="card-body bg1">
            <p class="card-text text-justify font-weight-light text-whiteg">We have employed accomplished technicians and graphic computer design artists working together in our studios to prepare your originals, scan them on the latest high-resolution</p>
            <p class="hvr-sweep-to-top btn text-whiteg" style="">PRODUCT LINES</p>
          </div>
        </div>
      </div>
    </div>

    <!--Certifications-->
    <div class="container-fluid p-0 ">
      <div class="parallax"><br><br>
        <h1 class="text-center text-gray shadow-text"  data-aos="fade-up" data-aos-once="true" data-aos-duration="1000" >CERTIFICATIONS</h1>
        <div class=" p-5 m-5" data-aos="fade-right" data-aos-once="true" data-aos-duration="3000" >
          <img src="image/cert1.png" class="image-fluid float-left" style="width:150px;">
          <p class="my-auto text-justify font-weight-light"> Packaging is a fundamental part of any brand. It defines product integrity, security and drives innovation. Consumers are attracted to strong and recognizable products that need to be consistent in size, colour and shape. Packaging is therefore an integral part of the manufacturing process.
            The BRC Global Standard for Packaging and Packaging Materials is the first Packaging Standard in the world to be recognized by the Global Food Safety Initiative (GSFI) benchmarking committee. It’s why over 3,500 suppliers in over 80 countries have chosen to be certificated to this scheme and is recommended or accepted by specifiers worldwide.
          The BRC Packaging Standard can be used by any manufacturer producing packaging materials for all types of products - from food to consumer products - at all levels: primary, secondary and tertiary.</p>
        </div>
        <div class="p-5 m-5" data-aos="fade-left" data-aos-once="true" data-aos-duration="3000" >
          <img src="image/cert2.png" class="image-fluid float-right" style="width:250px;">
          <p class="my-auto text-justify font-weight-light"> ISO 9001 is the international standard that specifies requirements for a quality management system (QMS). Organizations use the standard to demonstrate the ability to consistently provide products and services that meet customer and regulatory requirements. It is the most popular standard in the ISO 9000 series and the only standard in the series to which organizations can certify.
          ISO 9001 was first published in 1987 by the International Organization for Standardization (ISO), an international agency composed of the national standards bodies of more than 160 countries. The current version of ISO 9001 was released in September 2015.</p>
        </div><br>
        <div class="p-5 m-5" data-aos="fade-right" data-aos-once="true" data-aos-duration="3000" >
          <img src="image/cert3.png" class="image-fluid float-left " style="width:250px;">
          <p class="my-auto text-justify font-weight-light">HALAL means that the product is permitted under the Shariah (Islamic) Law. This law is based on Qu’ran Hadith (Tradition of Prophet Muhammad),Ijma (consensus) and Qiyas (deduction by analogy). The Qiyas approved by the relevant Islamic Authority also fulfills the following conditions.<br><br>

          1. Does not contain any components or product of animals that is not HALAL.<br>

          2. Does not contain any ingredients that is considered najis(filthy).<br>

          3. During its preparation, processing or manufacturing, the tools or equipment used do not contaminate the product with things which are considered najis.<br><br>

          HALAL Certification is a certificate of compliance of the religious requirement observed by all Muslim worldwide. It is now acceptable scientifically that a HALAL certificate is a guarantee of genuineness, best quality, wholesomeness, cleanliness, and best fit for human consumption of the food product.</p>
        </div>
      </div>
    </div>

    <?php include('footer.php') ?>
    <?php include('layout/script.php') ?>
</body>
</html>