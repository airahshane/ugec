<!--footer-->
  <div class="container-fluid p-3  text-white font-weight-light bg1" > <!--bg-dark-->
    <div class="row p-3" style="color: #dde1dc;">
      <div class="col-md-4 item shadow-text"  data-aos="fade-right"  data-aos-once="true"  data-aos-duration="1700">
        <h3 style="">CONTACT US</h3><br>
        <i class="fas fa-phone icon-color"></i> (632) 8531 8888<br>
        <i class="fas fa-envelope icon-color"></i> salesandmarketing@ugecorp.com<br>
        <i class="fab fa-facebook-square icon-color"></i> United Graphics Expression Corporation
      </div>
      <div class="col-md-4 shadow-text" data-aos="fade-right" data-aos-once="true" data-aos-duration="1700">
        <h3>CAVITE</h3><br>
        <i class="fas fa-map-marker-alt icon-color"></i> UGEC Corporate Centre, Governor's Drive, Dasmariñas Technopark, Dasmariñas, Cavite
      </div>
      <div class="col-md-4 shadow-text" data-aos="fade-right" data-aos-once="true" data-aos-duration="1700">
        <h3>MAKATI</h3><br>
        <i class="fas fa-map-marker-alt icon-color"></i> 26th Floor Unit A, Trafalgar Plaza, 105 H.V. Dela Costa Street, Salcedo Village, Makati City
      </div>
    </div>
  </div>