<!DOCTYPE html>
<html lang="en">
<?php include('layout/head.php') ?>
<style type="text/css">

.bg1 {
  background: url(image/bg1.png);
}

section{
  height: 500px;
  background-image: url('image/about.jpg');
  background-attachment: fixed;
  background-position: center;
  background-repeat: repeat;
  background-size: cover;
  box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.1);
}

/*.c {
  padding: 15px;
  margin-top: 30px;
  box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.1);
}

.c img {
  width: 100%;
  object-fit: cover;
  border-radius: 3px;
  box-shadow: 0 3px 20px 11px rgba(0, 0, 0, 0.09);
}

.c .top-sec {
  margin-top: -30px;
  margin-bottom: 15px;
}*/

.top-sec {
  font-weight: 350;
  background: url(image/bg1.png);
  text-shadow: 2px 4px 3px rgba(0,0,0,0.3);
}

/*.map-container-5{
  overflow:hidden;
  padding-bottom:56.25%;
  position:relative;
  height:0;
}
.map-container-5 iframe{
  left:0;
  top:0;
  height:100%;
  width:100%;
  position:absolute;
}

.ugec{
  text-shadow: 2px 2px 15px #3A3A3A;
  font-weight: 700;
  font-size:14vw;
}

.full{
  text-shadow: 2px 2px 2px #3A3A3A;
  font-weight: 700;
  font-size:4vw;
}*/

.parallax{
  /* The image used */
  background-image: url('image/locback.jpg');

  /* Full height */
  height: 100%;  

  /* Create the parallax scrolling effect */
  background-attachment: fixed;
  background-position: center;
  background-repeat: repeat;
  background-size: cover;
}
</style>

<body class="body-color">
  <?php include('layout/navbar.php') ?>

  <div class="container-fluid p-0">
    <section class="container-fluid"><br>
      <div class="row align-items-center h-75">
        <div class="mx-auto">
          <div class="  container">
            <div class="text-white text-center" data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">
              <h1 style="font-size:;" class=" align-items-center align-bottom ugec" >U. G. E. C.</h1>
              <h3 class="align-middle full" data-aos="fade-up" >United Graphics Expression Corporation</h3>
            </div>
          </div>  
        </div>
      </div>  
    </section>
  </div>

  <div class="parallax">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="card c ">
            <div class="top-sec" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1300">
              <h1 class="text-white text-center" >CAVITE</h1>
            </div>
            <div id="map-container-google-8" class="z-depth-1-half map-container-5" style="height: 400px"  data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15465.214799852492!2d120.98298848927922!3d14.293778746389028!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397d606622a9e7d%3A0x8804af0b0645b608!2sUnited%20Graphic%20Expression%20Corporation!5e0!3m2!1sen!2sph!4v1593737309370!5m2!1sen!2sph"
            frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="card c">
            <div class="top-sec" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1300">
              <h1 class="text-white text-center">MAKATI</h1>
            </div>
            <div id="map-container-google-8" class="z-depth-1-half map-container-5" style="height: 400px"  data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.673912238706!2d121.0240189489539!3d14.560630781912058!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c90717e6e71d%3A0x434ff9f34929c72d!2sTrafalgar%20Plaza%20Building!5e0!3m2!1sen!2sph!4v1593737626536!5m2!1sen!2sph" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div><br>
    </div>
  </div>

  <?php include('footer.php') ?>
  <?php include('layout/script.php') ?>      

</body>
</html>