<nav class="navbar navbar-expand-sm navbar-light sticky-top textsize bg1"   data-toggle="affix" style="border-bottom:#b07800 solid 2px;"> <!-- bg-light -->
  <div class="mx-auto d-sm-flex d-block flex-sm-nowrap" data-aos="fade-down"  data-aos-once="true"  data-aos-duration="1650">
    <div class="navbar-brand" href="#"> <img src="image/logo.png"> </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample11" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse text-center navigation-bar" id="navbarsExample11"  >
      <div class="menu-nav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link font-weight-light" href="index.php" style="color:#afb0b2;">HOME</a>
          </li>
          <li class="nav-item font-weight-light">
            <a class="nav-link" href="prodLines.php" style="color:#afb0b2;">PRODUCT LINES</a>
          </li>
          <li class="nav-item font-weight-light">
            <a class="nav-link" href="ourLocation.php" style="color:#afb0b2;">OUR LOCATION</a>
          </li>
          <li class="nav-item font-weight-light">
            <a class="nav-link" href="aboutUs.php" style="color:#afb0b2;">ABOUT US</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>