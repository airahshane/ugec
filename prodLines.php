<!DOCTYPE html>
<html lang="en">
<?php include('layout/head.php') ?>
<style type="text/css">
  .laptop {
  /* The image used */
  background-image: url('image/laptop.jpg');

  /* Full height */
  height: 100%;  

  /* Create the parallax scrolling effect */
  background-attachment: fixed;
  background-position: center;
  background-repeat: repeat;
  background-size: cover;
}

/*.expertise{
border: #fcba03 2px solid;
background: #fcba03 ;
color: white;
}*/
.bg1 {
   background: url(image/bg1.png);
}
.pl1 {
  background:url(image/PL1.jpg);
  size: 50%;
}
/* Slider */
.slick-slide {
  margin: 0px 20px;
}
.slick-slide img {
  width: 100%;
}
.slick-slider {
  position: relative;
  display: block;
  box-sizing: border-box;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-touch-callout: none;
  -khtml-user-select: none;
  -ms-touch-action: pan-y;
  touch-action: pan-y;
  -webkit-tap-highlight-color: transparent;
}
.slick-list {
  position: relative;
  display: block;
  overflow: hidden;
  margin: 0;
  padding: 0;
}
.slick-list:focus {
   outline: none;
}
.slick-list.dragging {
    cursor: pointer;
    cursor: hand;
}
.slick-slider .slick-track,
.slick-slider .slick-list {
  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
  -o-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
}
.slick-track {
  position: relative;
  top: 0;
  left: 0;
  display: block;
}
.slick-track:before,
.slick-track:after {
  display: table;
  content: '';
}
.slick-track:after {
  clear: both;
}
.slick-loading .slick-track {
  visibility: hidden;
}
.slick-slide {
  display: none;
  float: left;
  height: 100%;
  min-height: 1px;
}
[dir='rtl'] .slick-slide {
  float: right;
}
.slick-slide img {
  display: block;
}
.slick-slide.slick-loading img {
  display: none;
}
.slick-slide.dragging img {
  pointer-events: none;
}
.slick-initialized .slick-slide {
    display: block;
}
.slick-loading .slick-slide
{
    visibility: hidden;
}
.slick-vertical .slick-slide
{
    display: block;
    height: auto;
    border: 1px solid transparent;
}
.slick-arrow.slick-hidden {
    display: none;
}


 /*Hover Fall Effect*/
.card {
  position: relative;
  /*font-weight: lighter;   */
}

.image {
  display: block;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
   opacity: 1.0;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
   width: 100%;
}

.card:hover .image {
  opacity: 0.2;
}

.card:hover .middle {
  opacity: 1;
  transition: all 0.2s ease-in-out 0.2s;
  margin-top: 10px;
  min-width: 400px;
}

.text {
  background-color: #32312d;
  color: white;         
  font-size: 20px;
  padding: 16px 32px;
 text-transform: uppercase;
}
</style>

<body class="body-color">
  <?php include('layout/navbar.php') ?>

  <!--Expertise-->
  <div class="container-fluid "><br><br><br>
    <h1 class="text-center" data-aos="fade-down" data-aos-once="true" data-aos-duration="1650" style="text-shadow: 2px 4px 3px rgba(0,0,0,0.3);">PRODUCT LINES</h1>
   
    <h5 class="text-center m-5 font-weight-light" style="line-height: 1.6;" data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">UGEC is a one-stop-shop, catering to the complexity and sophistication of our customer’s needs. It encompasses the aspects of print and packaging solutions from design conceptualization to execution. With the diversity of the products and services that the company can offer we have organized ourselves into three strategic business units: Offset, Rotogravure and Print Media. In so doing, we have created focus in the formation and implementation of our strategies, promoted synergy in the workplace and streamlined supply chain activities to achieve operational excellence.</h5><br> 
  </div>

  <div class="container-fluid p-0 ">
    <div class="laptop">
      <div class="container-fluid ">
        <br>

          <div class="card-deck">
            <div class="card img" style="  box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.30);" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1650">
              <img class="card-img image " src="image/PL1.jpg">
              <div class="card-body text-center middle">
                <p class="card-title text">PACKAGING MATERIALS</p>
                <em>Packaging Labels & Boxes • Flexible Laminates • Blister Foil and Flex Foil • Cup Lids • Ice Cream Cone Sleeves • Shrink Sleeves (PVC, PET) • Cap Seals and Seal-O-Bands • Flexible Laminates</em>
              </div>
            </div><br>

            <div class="card img" style="  box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.1);" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1650">
              <img class="card-img image " src="image/PL4.jpg">
              <div class="card-body text-center middle">
                <p class=" card-title text">PRINT MEDIA</p>
                <em>The finest available pre-sensitized printing plates are used for press output, produced by the best exposure devices. The quality of our plate output was further enhanced with the acquisition of a Computer-to-Plate top-of-the-line equipment which allows us to do direct imaging on plates.</em>
              </div>
            </div><br>

            <div class="card img" style="  box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.1);" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1650">
              <img class="card-img image" src="image/PL2.png">
              <div class="card-body text-center middle">
                 <p class=" text">ADVERTISING AND PROMO MATERIALS</p>
                <em>Posters & Banners • Flyers & Leaflets • Tent Cards • Sales & Direct Mail Kits • Promo & Drop Boxes • Shelf Cards • Header Cards • Coupons & Sticker • Recipe Booklets • Paper Bags • Hang Tags • Gift Wrappers</em>
              </div>
            </div>
          </div><br>

          <div class="card-deck"><br>
            <div class="card img" style="  box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.1);" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1650">
              <img class="card-img image " src="image/PL8.png">
              <div class="card-body text-center middle">
                <p class=" text">PRE-MEDIA</p>
                <em>Product Shooting • Color Separation • CD Writing • Re-touching</em>
              </div>
            </div><br>

            <div class="card " style="  box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.1);" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1650">
              <img class="card-img image " src="image/PL3.png">
              <div class="card-body text-center middle">
                <p class=" text">COLLATERALS AND OTHERS</p>
                <em>Calendars • Annual Reports • Coffee Table Books • Newsletters • Invitation Cards • Letterheads & Envelopes</em>
              </div>
            </div><br>

            <div class="card " style="  box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.1);" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1650">
              <img class="card-img image " src="image/PL7.png">
              <div class="card-body text-center middle">
                <p class=" text">GRAPHIC DESIGN AND<br>DIGITAL PRE-PRESS</p>
                <em>Packaging Labels & Boxes • Flexible Laminates • Blister Foil and Flex Foil • Cup Lids • Ice Cream Cone Sleeves • Shrink Sleeves (PVC, PET) • Cap Seals and Seal-O-Bands • Flexible Laminates</em>
              </div>
            </div><br>
          </div><br>

        </div>
      </div>
    </div><br><br>

    <div class="container-fluid" >
      <h1 class="text-center" data-aos="fade-up" data-aos-once="true" data-aos-duration="1650"  style="text-shadow: 2px 4px 3px rgba(0,0,0,0.3);">AWARDS</h1><br><br>
      <section class="customer-logos slider" data-aos="zoom-in" data-aos-once="true" data-aos-duration="1650">
        <div class="slide"><img src="image/award/1.png"></div>
        <div class="slide"><img src="image/award/2.png"></div>
        <div class="slide"><img src="image/award/3.png"></div>
        <div class="slide"><img src="image/award/4.png"></div>
        <div class="slide"><img src="image/award/6.png"></div>
        <div class="slide"><img src="image/award/7.png"></div>
        <div class="slide"><img src="image/award/8.png"></div>
      </section>
    </div><br><br>

    <?php include('footer.php') ?>
    <script type="text/javascript">$(document).ready(function(){
      $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 4
          }
        }, {
          breakpoint: 520,
          settings: {
            slidesToShow: 3
          }
        }]
      });
    });</script>
    <?php include('layout/script.php') ?>
      
</body>
</html>