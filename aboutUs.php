<!DOCTYPE html>
<html lang="en">
<?php include('layout/head.php') ?>
<style type="text/css">
.torn{
  background-image: url('image/torn2.jpg');

  /* Full height */
  height: 100%;  

  /* Create the parallax scrolling effect */
  background-attachment: fixed;
  background-position: center;
  background-repeat: repeat;
  background-size: cover;
}

.bg1 {
  background: url(image/bg1.png);
}
</style>

<body class="body-color">
  <?php include('layout/navbar.php') ?>

  <!--Slideshow-->
  <div class="jumbotron jumbotron-fluid bg-dark mb-0">
    <div class="jumbotron-background">
      <img src="image/about.jpg" class="blur">
    </div>
    <div class="container text-white p-3" data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">
      <h4 >UNITED GRAPHIC EXPRESSION CORPORATION (UGEC)</h4><br>
      <p class="lead text-justify" style="font-size: 17px;">a full-service printing and packaging company. It started its operations August 18, 1988 in Malabon. Since then, it has continuously grown and expanded its capabilities to meet the needs of its current and future business partners in the industry.<br><br>
      The company’s resources are now fully operational in Dasmarinas, Cavite. Fulfilling printing and packaging needs go beyond producing quality work. It is a constant research and acquisition of leading edge technologies. It is maintaining and uniting old world craftsmanship with new generation ideas and capabilities. UGEC is a company that welcomes the opportunity to pursue new and progressive printing and packaging solutions for its clients. As a testament to its commitment for continuous improvement UGEC has achieved considerable milestones. One of which is the Print Excellence Award from the Printing Industries Association of the Philippines, Inc. won for six consecutive years from 1996 to 2001 in recognition for the company’s ability to consistently produce quality prints. We are likewise an ISO 9001:Certified company.</p>
    </div><!-- /.container -->
  </div><!-- /.jumbotron -->

  <!--Mission & Vision-->
  <div class="container-fluid p-0 m-0">
    <div class="card-group rounded-0"> 
      <div class="card text-whiteg">
        <div class="card-body bg1" >
          <div class="m-1" data-aos="flip-right" data-aos-once="true" data-aos-duration="1650">
            <h2 class=" text-center text-whiteg shadow-text">OUR MISSION</h2>   
            <hr class="text-whiteg shadow-img" style="border-top: #dde1dc solid;width: 250px;height: -1px;">
            <p class="card-text text-justify p-5 font-weight-light text-whiteg">We are a dynamic design, commercial printing and packaging solutions partner and employer of choice committed to:<br><br>
            •  Provide our CUSTOMERS creative means of communication that strengthen their brands and assures integrity of the products through high level of security in the operations<br>
            • Provide our EMPLOYEES opportunities for them to grow and develop professionally and personally<br>
            • Create value for our SHAREHOLDERS and assure them of a fair and equitable return of investment<br>
            • Develop and grow a strong and sustainable relationship with our SUPPLIERS, and<br>
            • Be an environment friendly and socially responsible PARTNER in COMMUNITY and NATION-BUILDING.</p>
          </div>
        </div>
      </div>
      <div class="card vision-card text-whiteg">
        <div class="card-body bg1" >
          <div class="m-1" data-aos="flip-left" data-aos-once="true" data-aos-duration="1650">
            <h2 class="text-center shadow-text">OUR VISION</h2>
            <hr class="text-whiteg shadow-img" style="border-top: #dde1dc solid;width: 250px;height: -1px;">
            <p class="card-text text-justify p-5 font-weight-light text-whiteg">By the year 2018, UGEC will be a publicly-listed and globally-known design, commercial and security printing and packaging solutions partner and employer of choice.<br><br>
            • Ensuring financial viability primarily through aggressive sales growth,<br>
            • Employing an effective integrated management system that promotes operational excellence,<br>
            • Ensuring a lean and agile organizational structure that guarantees sustainable progress,<br>
            • Maintaining and investing on human assets that drive growth in the business, and<br>
            • Assuring shareholders a fair and reasonable return on equity.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--Certifications-->
  <div class="container-fluid p-0 font-weight-normal ">
    <div class="torn">
      <div class="p-5">
        <h1 class="text-center shadow-text" data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">OUR SHARED VALUES</h1><br>
        <div class="row p-5 text-center">
          <div class="col-md-4" data-aos="zoom-out" data-aos-once="true" data-aos-duration="1000">
            <i class="fas fa-check-double fa-5x hvr-float" style="filter: drop-shadow(5px 5px 5px #222);"></i><br>
            <h5 class="p-3">INTEGRITY</h5>
            <p>Unity of thoughts, words and actions that are translated into favorable actions and expected results.</p><br>  
          </div>
          <div class="col-md-4" data-aos="zoom-out" data-aos-once="true" data-aos-duration="1000">
            <i class="fas fa-teeth fa-5x hvr-float" style="filter: drop-shadow(5px 5px 5px #222);"></i><br>
            <h5 class="p-3">TEAMWORK</h5>
            <p>The dependability of everyone to dynamically work together to achieve a common goal.</p><br> 
          </div>
          <div class="col-md-4" data-aos="zoom-out" data-aos-once="true" data-aos-duration="1000">
            <i class="fas fa-child fa-5x hvr-float" style="filter: drop-shadow(5px 5px 5px #222);"></i><br>
            <h5 class="p-3">CUSTOMER FOCUS</h5>
            <p>Unity of thoughts, words and actions that are translated into favorable actions and expected results.</p><br> 
          </div>
        </div>

        <div class="row p-5 text-center">
          <div class="col-md-3" data-aos="zoom-out" data-aos-once="true" data-aos-duration="1000">
            <i class="fas fa-flag-checkered fa-5x hvr-float" style="filter: drop-shadow(5px 5px 5px #222);"></i><br>
            <h5 class="p-3">PASSION FOR EXCELLENCE</h5>
            <p>Unity of thoughts, words and actions that are translated into favorable actions and expected results.</p><br>
          </div> <br>
          <div class="col-md-3" data-aos="zoom-out" data-aos-once="true" data-aos-duration="1000">
            <i class="fas fa-cogs fa-5x hvr-float" style="filter: drop-shadow(5px 5px 5px #222);"></i><br>
            <h5 class="p-3">COMMITMENT <br>TO GOAL</h5>
            <p>Unity of thoughts, words and actions that are translated into favorable actions and expected results.</p><br>
          </div><br>
          <div class="col-md-3" data-aos="zoom-out" data-aos-once="true" data-aos-duration="1000">
            <i class="far fa-lightbulb fa-5x hvr-float" style="filter: drop-shadow(5px 5px 5px #222);"></i><br>
            <h5 class="p-3">CREATIVITY <br>AND INNOVATION</h5>
            <p>Challenging the status quo, thinking outside  the box, and finding better and logical ways to evolve.</p><br>
          </div><br><br>
          <div class="col-md-3" data-aos="zoom-out" data-aos-once="true" data-aos-duration="1000">
            <i class="fas fa-sign fa-5x hvr-float" style="filter: drop-shadow(5px 5px 5px #222);"></i><br>
            <h5 class="p-3">SOCIAL<br> RESPONSIBILITY</h5>
            <p>A selfless ethical contribution towards the welfare and interests of its people and the society in which it operates.</p>
          </div> 
        </div>
      </div> 
    </div>
  </div>

  <div class="container-fluid "><br><br><br>
    <h1 class="text-center shadow-text" data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">OUR QUALITY POLICY: A SHARED COMMITMENT</h1>
    <h5 class="text-center m-5 font-weight-light" style="line-height: 1.6;" data-aos="fade-up" data-aos-once="true" data-aos-duration="1650">We at United Graphic Expression Corporation are committed to achieve and sustain market leadership in design, commercial and security printing and packaging solutions.We are creative, innovative and passionate in delivering the best in quality and service to meet or exceed our customers’ as well as legal requirements. Our commitment to quality is for continual improvement of all processes.<br><br>
      We will strive to meet our goals using appropriate technologies, collaborating with our stakeholders in a mutually respectful environment toward the common purpose of sustained growth.<br><br>
      We are committed to continually develop a competent workforce and drive a high performance culture.<br><br>
      We shall maintain the highest moral, ethical and legal standards in all of our business dealings.<br><br>
      These commitments and the equivalent quality objectives and measures shall be reviewed for continuing suitability and shall be communicated for full understanding within the organization All these will bring about total customer satisfaction.<br><br>
      United Graphic Management and its employees are one in sharing this commitment.<br>
    Quality is everyone’s responsibility.</h5><br>
  </div>

  <?php include('footer.php') ?>
  <?php include('layout/script.php') ?>
</body>
</html>